import socket
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1>Hello, World</h1><p> from {} </p>'.format(socket.gethostbyname(socket.gethostname()))
